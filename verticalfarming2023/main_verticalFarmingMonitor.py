def main():
    from machine import Pin, ADC, I2C
    from time import sleep
    import network
    import urequests as requests
    from umqttsimple import MQTTClient
    import ubinascii
    import gc
    import BME280
    
    esp.osdebug(None)

    def restart_and_reconnect():
        global client_id, mqtt_server
        print('Failed to connect to MQTT broker. Reconnecting...')
        time.sleep(10)
        machine.reset()

    gc.collect()

    client_id = ubinascii.hexlify(machine.unique_id())

    topic_pub_temp = b'esp/verticalFarm/sensorData'

    last_message = 0
    message_interval = 5

    station = network.WLAN(network.STA_IF)

    station.active(True)
    station.connect(ssid, password)

    print('Connection successful')

    def connect_mqtt(client_id, mqtt_server):
        client = MQTTClient(client_id, mqtt_server)
        client.connect()
        print('Connected to %s MQTT broker' % (mqtt_server))
        return client

    ks0049 = ADC(Pin(34))
    ks0049.atten(ADC.ATTN_11DB)

    i2c = I2C(scl=Pin(22), sda=Pin(21), freq=10000)

    wifi_ssid = "" # insert wifi ssid
    wifi_password = "" # insert wifi password
    webhook_url = "https://maker.ifttt.com/trigger/{verticalFarming}/json/with/key/jTRb36Bqfy1LpEaRrpXUgt-8CAtFSRjQbcbEocllyy2" # insert IFTTT webhook URL

    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    sta_if.connect(wifi_ssid, wifi_password)
    while not sta_if.isconnected():
        print(".", end = "")

    while True:
        # Soil moisture
        ks0049_value = ks0049.read()
        print(ks0049_value)
        sleep(1)

        # Temperature, humidity and atmospheric pressure
        bme = BME280.BME280(i2c=i2c)
        temp = bme.temperature
        hum = bme.humidity
        pres = bme.pressure
        print('Temperature: ', temp)
        print('Humidity: ', hum)
        print('Pressure: ', pres)
        sleep(1)

        client = connect_mqtt(client_id, mqtt_server)
        client.publish(topic_pub_temp, temp, hum, pres, ks0049_value)
        time.sleep(1)

        url = webhook_url + "?value1=" +  str(temp) + "&value2=" + str(humid) + "&value3=" + str(pres) + "&value4=" + str(ks0049_value)
    
        try:
            r = requests.get(url)
            print(r.text)
        except Exception as e:
            print(e)
        sleep(300)
if __name__ == '__main__':
    main()
