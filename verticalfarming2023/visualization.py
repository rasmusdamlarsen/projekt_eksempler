import sqlite3
import pandas as pd
import streamlit as st
import plotly.express as px
import time 
import plotly.graph_objects as go



# Function to fetch data from SQLite database
def get_data():
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute("SELECT Timestamps, Temperature, Humidity, Pressure FROM BME280")
    data = cur.fetchall()

    cur.close()
    conn.close()

    df = pd.DataFrame(
        data,
        columns=["Timestamps", "Temperature", "Humidity", "Pressure"],
    )

    return df

def get_data2():
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute("SELECT LDR_1, LDR_2, LDR_3, LDR_4, LDR_5, LDR_6 FROM Lys")
    data = cur.fetchall()

    cur.close()
    conn.close()

    df2 = pd.DataFrame(
        data,
        columns=['LDR_1', 'LDR_2', 'LDR_3', 'LDR_4', 'LDR_5', 'LDR_6']
    )
    return df2

def get_data3():
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute("SELECT Timestamps, Højde FROM Højde")
    data = cur.fetchall()

    cur.close()
    conn.close()

    df3 = pd.DataFrame(
        data,
        columns=['Timestamps', 'Højde']
    )
    return df3

def get_data4():
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute("SELECT Timestamps, Resovoir, Bakker FROM Vandstand")
    data = cur.fetchall()

    cur.close()
    conn.close()

    df4 = pd.DataFrame(
        data,
        columns=['Timestamps', 'Resovoir', 'Bakker']
    )
    return df4

def get_data5():
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute("SELECT Timestamps, Moisture FROM Jordfugtighed")
    data = cur.fetchall()

    cur.close()
    conn.close()

    df5 = pd.DataFrame(
        data,
        columns=['Timestamps', 'Moisture']
    )
    return df5

st.set_page_config(layout="wide", page_title='Vertical Farming', page_icon='🌻', )

def update():
    # Define update intervals
    update_intervals = {
        "10 seconds": 10,
        "30 seconds": 30,
        "60 seconds": 60
    }

    # Select update interval
    update_interval = st.selectbox("Update Interval", list(update_intervals.keys()))

    return update_interval, update_intervals

def page1():
    # CSS styling for the containers
    title_container_style = """
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-bottom: 20px;
    """

    data_container_style = """
        display: flex;
        flex-direction: row;
        justify-content: center;
        gap: 20px;
        margin-bottom: 20px;
    """
    # Define the square style
    square_style = """
        position: relative;
        overflow: visible;
        width: 200px;
        height: 100px;
        background-color: rgba(29, 29, 29, 1);
        border: 2px solid rgba(179, 129, 241, 1);
        border-radius: 5px;
        margin-bottom: 10px;
        padding: 10px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    """
    # Add content specific to Page 2
    # Fetch the data from the database
    df = get_data()
    df2 = get_data2()
    df3 = get_data3()
    df4 = get_data4()
    df5 = get_data5()
    # Get the last row of data
    last_row = df.iloc[-1] if not df.empty else None
    last_row2 = df2.iloc[-1] if not df2.empty else None
    last_row3 = df3.iloc[-1] if not df3.empty else None
    last_row4 = df4.iloc[-1] if not df4.empty else None
    last_row5 = df5.iloc[-1] if not df5.empty else None

    # Render the title section
    st.markdown(
        f'<div style="{title_container_style}">'
        f'   <h1 style="text-align: center; color: white;">Vertical farming</h1>'
        f'   <h2 style="text-align: center; color: white;">Live Data</h2>'
        f'   <h3 style="text-align: center; color: white;">Current Measurements</h3>'
        f'</div>',
        unsafe_allow_html=True
    )
    
    col1, col2 = st.columns(2)
    
   # Render the data section
    st.markdown(
        f'<div style="{data_container_style}">'
        f'   <div style="display: flex; flex-direction: column; align-items: center;">'
        f'       <div style="display: flex; flex-direction: row;">'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Temperature:</div>'
        f'               <div style="font-size: 24px;">{last_row["Temperature"]} °C</div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Humidity:</div>'
        f'               <div style="font-size: 24px;">{last_row["Humidity"]} %</div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Pressure:</div>'
        f'               <div style="font-size: 24px;">{last_row["Pressure"]} hPa</div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Height:</div>'
       # f'               <div style="font-size: 24px;">{last_row3["Højde"]} mm</div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Water Level - Resovoir:</div>'
        f'               <div style="font-size: 24px;">{last_row4["Resovoir"]} mm</div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Water Level - Bakker:</div>'
        f'               <div style="font-size: 24px;">{last_row4["Bakker"]} mm</div>'
        f'           </div>'
        f'           <div style="{square_style}">'
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">Soil Moisture:</div>'
        f'               <div style="font-size: 24px;">{last_row5["Moisture"]} %</div>'
        f'           </div>'
        f'       </div>'
        f'       <div style="display: flex; flex-direction: row;">'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 1:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_1"]} </div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 2:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_2"]} </div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 3:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_3"]} </div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 4:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_4"]} </div>'
        f'           </div>'
        f'           <div style="{square_style}; margin-right: 10px;">'  # Add margin-right for spacing
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 5:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_5"]} </div>'
        f'           </div>'
        f'           <div style="{square_style}">'
        f'               <div style="font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);">LDR 6:</div>'
        f'               <div style="font-size: 24px;">{last_row2["LDR_6"]} </div>'
        f'           </div>'
        f'       </div>'
        f'   </div>'
        f'</div>',
        unsafe_allow_html=True
    )

# Function to create line chart with custom layout
def create_line_chart(df, x_col, y_col, y_title, line_color):
    fig = px.line(df, x=x_col, y=y_col)
    fig.update_layout(
        yaxis=dict(title_text=y_title, color="#9C9C9C"),
        xaxis=dict(title_text=x_col, color="#9C9C9C"),
        plot_bgcolor="#1f1f1f",
        font_color="#9C9C9C",
        legend_bgcolor="#1f1f1f",
        title={
            'text': f"Real-Time {y_col} Visualization",
            'y':0.95,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        },
        legend_title='Variable'
    )
    fig.update_traces(line=dict(color=line_color))
    return fig

def create_heatmap(df, color_scale):
    fig = go.Figure(data=go.Heatmap(
        z=df.values,
        x=df.columns,
        y=df.index,
        colorscale=color_scale
    ))

    fig.update_layout(
        plot_bgcolor="#1f1f1f",
        font_color="#9C9C9C",
        title={
            'text': f"Heatmap Visualization",
            'y':0.95,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        },
    )

    return fig

def create_line_chart2(df, x_col, y_cols, y_titles, line_colors):
    fig = go.Figure()

    for y_col, y_title, line_color in zip(y_cols, y_titles, line_colors):
        fig.add_trace(go.Scatter(
            x=df[x_col],
            y=df[y_col],
            mode='lines',
            name=y_title,
            line=dict(color=line_color)
        ))

    fig.update_layout(
        yaxis=dict(title_text='Y Axis Title', color="#9C9C9C"),
        xaxis=dict(title_text='X Axis Title', color="#9C9C9C"),
        plot_bgcolor="#1f1f1f",
        font_color="#9C9C9C",
        legend_bgcolor="#1f1f1f",
        title={
            'text': "Multiple Line Chart",
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        },
        legend_title='Variable'
    )

    return fig

def page2():

    # Set up your Streamlit app
    #st.title('Virtical Farming')

    # Customizing the title layout
    st.markdown("<h1 style='text-align: center; color: white;'>Vertical Farming</h1>", unsafe_allow_html=True)
    st.markdown("<h2 style='text-align: center; color: white;'>Temperature, Humidity and Atmospheric Pressure</h2>", unsafe_allow_html=True)

    # Get data from the SQLite database
    df = get_data()

    # Define update intervals
    update_intervals = {
        "10 seconds": 10,
        "30 seconds": 30,
        "60 seconds": 60
    }

    # Select update interval
    update_interval = st.selectbox("Update Interval", list(update_intervals.keys()))

    # Plot the line charts side by side
    col1, col2 = st.columns(2)

    # Plot the initial line charts
    with col1:
        #st.subheader('Temperature')
        fig1 = create_line_chart(df, "Timestamps", "Temperature", "Temperature (Celsius)", "#9C9C9C")
        chart1 = st.plotly_chart(fig1, use_container_width=True)

    with col2:
        #st.subheader('Humidity')
        fig2 = create_line_chart(df, "Timestamps", "Humidity", "Humidity (%)", "red")
        chart2 = st.plotly_chart(fig2, use_container_width=True)

    # Plot the last line chart below the left chart
    col1_2, _ = st.columns([1, 1])
    with col1_2:
        #st.subheader('Atmospheric Pressure')
        fig3 = create_line_chart(df, "Timestamps", "Pressure", "Atmospheric Pressure", "green")
        chart3 = st.plotly_chart(fig3, use_container_width=True)

    # Continuously update the charts with new data
    while True:
        # Fetch updated data
        new_data = get_data()

        # Update the existing DataFrame
        df = pd.concat([df, new_data], ignore_index=True)

        # Update the line charts with new data
        fig1 = create_line_chart(df, "Timestamps", "Temperature", "Temperature (Celsius)", "#9C9C9C")
        fig2 = create_line_chart(df, "Timestamps", "Humidity", "Humidity (%)", "red")
        fig3 = create_line_chart(df, "Timestamps", "Pressure", "Atmospheric Pressure", "green")

        # Update the Streamlit charts with new figures
        chart1.plotly_chart(fig1, use_container_width=True)
        chart2.plotly_chart(fig2, use_container_width=True)
        chart3.plotly_chart(fig3, use_container_width=True)

        # Sleep for the specified update interval
        #time.sleep(update.update_intervals()[update.update_interval()])

def page3():
    #st.title("LDR")
    st.markdown("<h1 style='text-align: center; color: white;'>Vertical Farming</h1>", unsafe_allow_html=True)
    st.markdown("<h2 style='text-align: center; color: white;'>Light & Height</h2>", unsafe_allow_html=True)
    # Add content specific to Page 3

    # Define update intervals
    update_intervals = {
        "10 seconds": 10,
        "30 seconds": 30,
        "60 seconds": 60
    }

    # Select update interval
    update_interval = st.selectbox("Update Interval", list(update_intervals.keys()))

    df2 = get_data2()
    df3 = get_data3()
    col3, col4 = st.columns(2)
    with col3:
        fig4 = create_heatmap(df2, "Purp")
        st.plotly_chart(fig4)

    with col4:
        fig5 = create_line_chart(df3, "Timestamps", "Højde", "Højde (mm)", "#9C9C9C")
        chart5 = st.plotly_chart(fig5, use_container_width=True)
        chart5.plotly_chart(fig5, use_container_width=True)

def page4():
    st.markdown("<h1 style='text-align: center; color: white;'>Vertical Farming</h1>", unsafe_allow_html=True)
    st.markdown("<h2 style='text-align: center; color: white;'>Water Level & Soil Moisture</h2>", unsafe_allow_html=True)

    # Define update intervals
    update_intervals = {
        "10 seconds": 10,
        "30 seconds": 30,
        "60 seconds": 60
    }

    # Select update interval
    update_interval = st.selectbox("Update Interval", list(update_intervals.keys()))

    df4=get_data4()
    df5=get_data5()

    col5, col6 = st.columns(2)

    with col5:
        #st.subheader('Temperature')
        
        fig6 = px.line(df4, x='Timestamps', y=['Resovoir', 'Bakker'])

        fig6.update_layout(
        yaxis=dict(title_text='Water Lever', color="#9C9C9C"),
        xaxis=dict(title_text='Timestamps', color="#9C9C9C"),
        plot_bgcolor="#1f1f1f",
        font_color="#9C9C9C",
        legend_bgcolor="#1f1f1f",
        title={
            'text': f"Real-Time Water Level Visualization",
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        },
        legend_title='Variable'
    )
        
        chart6 = st.plotly_chart(fig6, use_container_width=True)
        chart6.plotly_chart(fig6, use_container_width=True)

    with col6:
        #st.subheader('Humidity')
        fig7 = create_line_chart(df5, "Timestamps", "Moisture", "Soil Moisture", "#9C9C9C")
        fig6.update_layout(
        yaxis=dict(title_text='Soil Moisture', color="#9C9C9C"),
        xaxis=dict(title_text='Timestamps', color="#9C9C9C"),
        plot_bgcolor="#1f1f1f",
        font_color="#9C9C9C",
        legend_bgcolor="#1f1f1f",
        title={
            'text': f"Real-Time SOil Moisture Visualization",
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        },
        legend_title='Variable'
    )
        chart5 = st.plotly_chart(fig7, use_container_width=True)
        chart5.plotly_chart(fig7, use_container_width=True)
        

# Create a navigation menu
nav_selection = st.sidebar.radio("Navigation", ("Live data", "BME280", "Light & Height", 'Water Level & Soil Moisture'))

# Apply custom CSS to the sidebar
sidebar_style = """
    background-color: rgba(29, 29, 29, 1);
    margin: 0px;
    padding: 10px;
    font-family: Segoe UI; font-size: 16px; color: rgba(137, 137, 137, 1);
"""

# Apply custom CSS to the sidebar radio buttons
radio_style = """
    .st-bp {
        background-color: rgba(179, 129, 241, 1) !important;
        color: white !important;
    }
"""

st.markdown(f'<style>.css-6qob1r.e1akgbir3 {{ {sidebar_style} }}</style>', unsafe_allow_html=True)
st.markdown(f'<style>{radio_style}</style>', unsafe_allow_html=True)

# Render the selected page
if nav_selection == "Live data":
    page1()
elif nav_selection == "BME280":
    page2()
elif nav_selection == "Light & Height":
    page3()
elif nav_selection == "Water Level & Soil Moisture":
    page4()