from dash import Dash, html, dcc
from dash.dependencies import Output, Input
import plotly.express as px
import sqlite3
import pandas as pd
import dash_bootstrap_components as dbc

# Henter data fra SQLite database
def get_data():
    # Åbner forbindelse
    conn = sqlite3.connect("plc.db")
    cur = conn.cursor()

    cur.execute(
        "SELECT Timestamp, Temperature_1, Temperature_2, Temperature_3, Temperature_4, Temperature_5, Temperature_6, Temperature_7, Flow_1, Flow_2 FROM plc_data"
    )
    data = cur.fetchall()

    # Lukker forbindelse
    cur.close()
    conn.close()

    # Opretter Pandas DataFrame med data fra database
    df = pd.DataFrame(
        data,
        columns=[
            "Timestamp",
            "Temperature_1",
            "Temperature_2",
            "Temperature_3",
            "Temperature_4",
            "Temperature_5",
            "Temperature_6",
            "Temperature_7",
            "Flow_1",
            "Flow_2",
        ],
    )
    return df


# Bruger Plotly Express til at lave en graf med DataFrame
fig = px.line(
    get_data(),
    x="Timestamp",
    y=[
        "Temperature_1",
        "Temperature_2",
        "Temperature_3",
        "Temperature_4",
        "Temperature_5",
        "Temperature_6",
        "Temperature_7",
        "Flow_1",
        "Flow_2",
    ],
)

def generate_footer():
    return html.Footer(
        dbc.Container(
            dbc.Row(
                dbc.Col(
                    html.P("", className="text-center text-muted"),
                    width={"size": 12}
                ),
            ),
        ),
        className="footer mt-auto py-3",
        style={"background-color": "#0E0E0E", "height": "489px"}
    )

    

# Laver Dash app og tilføjer grafen
app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title = "HeatGuardian"

app.layout = html.Div(
    style={"backgroundColor": "#0E0E0E", },
    children=[
        html.H1(
            "HeatGuardian",
            className="title",
            style={
                "textAlign": "center",
                "color": "#ffbc22",
                "font-family": "Bahnschrift",
                "margin": "0",
                "padding": "10px",
            },
        ),
        
        html.Footer(style={
            'height': '100%', 
            'backgroundcolor': '#1f1f1f'
            },
        ),
        dcc.Graph(id="live-update-graph", figure=fig),
        dcc.Interval(
            id="interval-component",
            interval=10 * 1000,  # Interval for opdatering i ms
            n_intervals=0,
        ),
        generate_footer(),
    ],
)

@app.callback(
    Output("live-update-graph", "figure"), [Input("interval-component", "n_intervals")]
)
def update_graph(n):
    df = get_data()
    fig = px.line(
        df,
        x="Timestamp",
        y=[
            "Temperature_1",
            "Temperature_2",
            "Temperature_3",
            "Temperature_4",
            "Temperature_5",
            "Temperature_6",
            "Temperature_7",
            "Flow_1",
            "Flow_2",
        ],
        
    )

    fig.update_layout(
        yaxis=dict(title_text="Temperature & Flow", color="#9C9C9C"),
        xaxis=dict(title_text="Timestamps", color="#9C9C9C"),
        plot_bgcolor="#1f1f1f",
        paper_bgcolor="#0E0E0E",
        font_color="#9C9C9C",
        legend_bgcolor="#1f1f1f",
        title={
            'text': "Real-Time Temperature & Flow Visualization",
            'y':0.95,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
            legend_title='Variable',
        
    )
    return fig

if __name__ == "__main__":
    app.run_server(debug=True)
