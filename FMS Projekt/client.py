from pymodbus.client.tcp import ModbusTcpClient
import sqlite3
from time import sleep
import datetime

# Connect to the Modbus TCP server
client = ModbusTcpClient("127.0.0.1", port=502)

db_path = 'plc.db'
db_conn = sqlite3.connect(db_path, check_same_thread=False)
print("Connection to database has been established...")
cur = db_conn.cursor()

while True:
    # Read the values from the Modbus registers
    holding_registers = client.read_holding_registers(99, 9, unit=1)

    # Check if the request was successful
    if holding_registers.isError():
        print("Modbus Error:", holding_registers)
    else:
        # Print the values
        print("Holding Registers:", holding_registers.registers)

    (
        temp1,
        temp2,
        temp3,
        temp4,
        temp5,
        temp6,
        temp7,
        flow1,
        flow2,
    ) = holding_registers.registers


    currentDateAndTime = datetime.datetime.now()
    timestamp = currentDateAndTime.strftime("%H:%M:%S")

    print("Timestamp:", timestamp)
    print(temp1, temp2, temp3, temp4, temp5, temp6, temp7, flow1, flow2)

    cur.execute("INSERT INTO plc_data(Timestamp, Temperature_1, Temperature_2, Temperature_3, Temperature_4, Temperature_5, Temperature_6, Temperature_7, Flow_1, Flow_2) VALUES (?,?,?,?,?,?,?,?,?,?)", (timestamp, temp1, temp2, temp3, temp4, temp5, temp6, temp7, flow1, flow2))

    db_conn.commit()
 
    sleep(10)