# MQTT subscriber_______________________________________________________________________________________________________________

# import paho.mqtt.client as mqtt


# # MQTT broker information
# broker_address = "192.168.195.19"  # Replace with your broker's IP or hostname
# broker_port = 1884  # Replace with your broker's port
# topic = 'esp/verticalFarm/sensorData/#'

# # Callback function for when the client receives a CONNACK response from the server
# def on_connect(client, userdata, flags, rc):
#     print("Connected to MQTT broker with result code: " + str(rc))
#     client.subscribe(topic)

# # Callback function for when a message is received from the server
# def on_message(client, userdata, msg):
#     print("Received message on topic: " + msg.topic)
#     print("Message payload: " + str(msg.payload))
#     # data = str(msg.payload.decode())
#     # print("Received message: " + str(msg.payload.decode()))
    
# # Create a new MQTT client instance
# client = mqtt.Client()

# # Assign callback functions
# client.on_connect = on_connect
# client.on_message = on_message

# # Connect to the MQTT broker
# client.connect(broker_address, broker_port)

# # Start the MQTT client loop (this is a blocking call that processes network traffic and dispatches callbacks)
# client.loop_forever()


#Streamlit__________________________________________________________________________________________________________________________________________________________

# import streamlit as st
# import pandas as pd
# import numpy as np
# import sqlite3

# st.title('Vertical Farming')
# DATA_COLUMN = 'date/time'
# # db_conn = ('D:\IT-teknolog\verticalfarming2023\vf-database.db') #insert path to database
# # DATA_PATH = db_conn = pd.read_sql_query('SELECT Temperature FROM BME280')


# def create_connection():
#     conn = sqlite3.connect('D:\IT-teknolog\verticalfarming2023\vf-database.db')
#     return conn

# conn = create_connection()

# @st.cache(allow_output_mutation=True)
# def load_data():
#     cursor = conn.cursor()
#     cursor.execute("SELECT Temperature FROM BME280")
#     data = cursor.fetchall()
#     return data

# data = load_data()


# def loadData(nrows):
#     data = pd.read_csv(data, nrows=nrows)
#     lowercase = lambda x: str(x).lower()
#     data.rename(lowercase, axis='columns', inplace=True)
#     data[DATA_COLUMN] = pd.to_datetime(data[DATA_COLUMN])
#     return data

# # Create a text element and let the reader know the data is loading.
# data_load_state = st.text('Loading data...')
# # Load 10,000 rows of data into the dataframe.
# data = loadData(10000)
# # Notify the reader that the data was successfully loaded.
# data_load_state.text('Loading data...done!')

#________________________________________________________________________________________________________________________________________________

import sqlite3
import pandas as pd
import numpy as np

# Henter data fra SQLite database
def get_data():
    # Åbner forbindelse
    conn = sqlite3.connect("vf-database.db")
    cur = conn.cursor()

    cur.execute(
        "SELECT Timestamp, Temperature FROM BME280"
    )
    data = cur.fetchall()

    # Lukker forbindelse
    cur.close()
    conn.close()

    # Opretter Pandas DataFrame med data fra database
    df = pd.DataFrame(
        data,
        columns=[
            "Timestamp",
            "Temperature",
            ],
    )
    return df


# #streamlit example code_____________________________________________________________________________________________________________________________________________
# import streamlit as st
# import time
# import numpy as np

# progress_bar = st.sidebar.progress(0)
# status_text = st.sidebar.empty()
# last_rows = np.random.randn(1, 1)
# chart = st.line_chart(last_rows)

# for i in range(1, 101):
#     new_rows = last_rows[-1, :] + np.random.randn(5, 1).cumsum(axis=0)
#     status_text.text("%i%% Complete" % i)
#     chart.add_rows(new_rows)
#     progress_bar.progress(i)
#     last_rows = new_rows
#     time.sleep(0.05)

# progress_bar.empty()

# # Streamlit widgets automatically run the script from top to bottom. Since
# # this button is not connected to any other logic, it just causes a plain
# # rerun.
# st.button("Re-run")






#_______________________________________________________________________________________________________________________________________________

# import paho.mqtt.client as mqtt

# # MQTT broker information
# broker_address = "192.168.42.19"  # Replace with your broker's IP or hostname
# broker_port = 1884  # Replace with your broker's port
# topic = 'esp/verticalFarm/sensorData/temp'

# # Callback function for when the client receives a CONNACK response from the server
# def on_connect(client, userdata, flags, rc):
#     print("Connected to MQTT broker with result code: " + str(rc))
#     client.subscribe(topic)

# # Callback function for when a message is received from the server
# def on_message(client, userdata, msg):
#     data = str(msg.payload.decode())
#     print("Received message: " + data)

# # Callback function for when a disconnect message is received from the server
# def on_disconnect(client, userdata, rc):
#     if rc != 0:
#         print("Unexpected disconnection.")

# try:
#     # Create a new MQTT client instance
#     client = mqtt.Client()

#     # Assign callback functions
#     client.on_connect = on_connect
#     client.on_message = on_message
#     client.on_disconnect = on_disconnect

#     # Connect to the MQTT broker
#     client.connect(broker_address, broker_port)

#     # Start the MQTT client loop (this is a blocking call that processes network traffic and dispatches callbacks)
#     client.loop_forever()

# except Exception as e:
#     print("An error occurred:", str(e))
