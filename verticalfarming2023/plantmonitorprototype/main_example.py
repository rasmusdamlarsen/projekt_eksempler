def main():
    from machine import Pin, ADC, I2C
    import machine
    import time
    import network
    import urequests as requests
    from umqttsimple import MQTTClient
    import ubinascii
    import gc
    import BME280

    terminal_debugging_output = True

    wifi_ssid = "OnePlus8Pro" # insert wifi ssid
    wifi_password = "ki97889788" # insert wifi password
    webhook_url = "https://maker.ifttt.com/trigger/VerticalFarming/with/key/jTRb36Bqfy1LpEaRrpXUgt-8CAtFSRjQbcbEocllyy2" # insert IFTTT webhook URL

    mqtt_server = "192.168.195.19" # insert broker ip
    client_id = ubinascii.hexlify(machine.unique_id())

    temperature = b'esp/verticalFarm/sensorData/temp'
    humidity = b'esp/verticalFarm/sensorData/hum'
    atmosphere = b'esp/verticalFarm/sensorData/pres'
    soilMoisture = b'esp/verticalFarm/sensorData/moist'

    ks0049 = ADC(Pin(34))
    ks0049.atten(ADC.ATTN_11DB)

    i2c = I2C(scl=Pin(22), sda=Pin(21), freq=10000)
    
    def restart_and_reconnect():
        global client_id, mqtt_server
        print('Failed to connect to MQTT broker. Reconnecting...')
        time.sleep(10)
        machine.reset()

    gc.collect()
    last_message = 0
    message_interval = 5

    station = network.WLAN(network.STA_IF)

    station.active(True)
    #station.connect(wifi_ssid, wifi_password)

    print('Connection successful')

    def connect_mqtt(client_id, mqtt_server):
        client = MQTTClient(client_id, mqtt_server)
        client.connect()
        print('Connected to %s MQTT broker' % (mqtt_server))
        return client

    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    sta_if.connect(wifi_ssid, wifi_password)

    while not sta_if.isconnected():
        print(".", end = "")
        print("\n")
        time.sleep(1)

    def take_measurements():
        # Soil moisture
        ks0049_value = ks0049.read()
        # Temperature, humidity and atmospheric pressure
        bme = BME280.BME280(i2c=i2c)
        temp = bme.temperature
        hum = bme.humidity
        pres = bme.pressure
        # Debugging output
        if terminal_debugging_output == True:
            print('Temperature: ', temp)
            print('Humidity: ', hum)
            print('Pressure: ', pres)
            print("Soil humidity: ", ks0049_value)
        return temp, hum, pres, ks0049_value

    while True:
        temp, hum, pres, ks0049_value = take_measurements()
        client = connect_mqtt(client_id, mqtt_server)
        client.publish(temperature, str(temp))
        client.publish(humidity, str(hum))
        client.publish(atmosphere, str(pres))
        client.publish(soilMoisture, str(ks0049_value))

        url = webhook_url + "?value1=" +  str(temp) + ',' + "&value2=" + str(ks0049_value) + ',' + "&value3=" + str(pres) 
        try:
            r = requests.get(url)
            print(r.text)
        except Exception as e:
            print(e)
        time.sleep(300)
if __name__ == '__main__':
    main()
