Attribute VB_Name = "Module2"
Function HexToText(hexValue As String) As String
    Dim hexParts() As String
    Dim i As Integer
    Dim textValue As String
    
    ' Split the input string into an array of hexadecimal parts
    hexParts = Split(Trim(hexValue), " ")
    
    ' Convert each part from hex to decimal and then to text
    For i = LBound(hexParts) To UBound(hexParts)
        If Len(hexParts(i)) Mod 2 = 0 Then
            textValue = textValue & ChrW("&H" & hexParts(i))
        Else
            HexToText = "Invalid Hex"
            Exit Function
        End If
    Next i
    
    HexToText = textValue
End Function

