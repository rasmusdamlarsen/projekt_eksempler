import sqlite3

db_path = "path" #insert path to database
db_conn = sqlite3.connect(db_path, check_same_thread=False)
print("Connected to database")
cur = db_conn.cursor()
#Value1 = temperature (float)
#Value2 = Humidity (float)
#Value3 = Moisture (float)
#Value4 = Pressure (float)
sql_statement = "INSERT INTO Measurements(column1, column2, column3, column4) VALUES (?, ?)" # SQlite3 SQL syntax
cur.execute(sql_statement, (value1, value2, value3, value4))
db_conn.commit()